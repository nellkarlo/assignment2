USE SuperheroesDb;

CREATE TABLE SuperheroPower
(
  SuperheroID INT NOT NULL,
  PowerID INT NOT NULL
);

ALTER TABLE SuperheroPower
ADD FOREIGN KEY (SuperheroID) REFERENCES Superhero(ID);

ALTER Table SuperheroPower
ADD FOREIGN KEY (PowerID) REFERENCES Power(ID);
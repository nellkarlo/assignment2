USE SuperheroesDb;

INSERT INTO Power (Name, Description)
VALUES ('Telekinesis', 'the supposed ability to move objects at a distance by mental power or other non-physical means.')

INSERT INTO Power (Name, Description)
VALUES('Flight','Ability to lift off the ground, to ride air currents or to fly self-propelled through the air.')

INSERT INTO Power (Name, Description)
VALUES('Shapeshifting', 'Ability to change appearance or body structure.')

INSERT INTO Power (Name, Description)
VALUES('Intangibility', 'The ability to alter your atoms in such a way that you can walk through walls.')


INSERT INTO SuperheroPower(SuperheroID, PowerID)
VALUES(1,2)

INSERT INTO SuperheroPower(SuperheroID, PowerID)
VALUES(1,4)

INSERT INTO SuperheroPower(SuperheroID, PowerID)
VALUES(1,1)

INSERT INTO SuperheroPower(SuperheroID, PowerID)
VALUES(2,1)

INSERT INTO SuperheroPower(SuperheroID, PowerID)
VALUES(3,1)

INSERT INTO SuperheroPower(SuperheroID, PowerID)
VALUES(3,3)
﻿using DatabasSQL.DataAccess;
using DatabasSQL.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabasSQL.Data_Access.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        /// <summary>
        /// Gets the Customer from database matching the ID provided.
        /// </summary>
        /// <param name="InputCustomerID">The customer ID used to query the database to find the Customer.</param>
        /// <returns>Found Customer. If not found returns Console Error message.</returns>
        public Customer GetCustomerById(int customerId)
        {
            Customer c = new Customer();
            try
            {
                using (SqlConnection connection = new SqlConnection(DbContext.GetConnectionString()))
                {
                    connection.Open();

                    String sql = $"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE {customerId} = CustomerId";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            Console.WriteLine(String.Format("{0,0} | {1,8} | {2,14} | {3,14} | {4,22}| {5,20} | {6,20}", reader.GetName(0), reader.GetName(1), reader.GetName(2), reader.GetName(3), reader.GetName(4), reader.GetName(5), reader.GetName(6)));
                            Console.WriteLine("-------------------------------------------------------------------------------------------------------------------------------");
                            if (!reader.HasRows) Console.WriteLine("Can't find any matches!");
                            
                            while (reader.Read())
                            {
                                c.CustomerId = reader.GetInt32(0);
                                c.FirstName = !reader.IsDBNull(reader.GetOrdinal("FirstName")) ? reader.GetString(1) : "<Missing first name>";
                                c.LastName = !reader.IsDBNull(reader.GetOrdinal("LastName")) ? reader.GetString(2) : "<Missing last name>";
                                c.Country = !reader.IsDBNull(reader.GetOrdinal("Country")) ? reader.GetString(3) : "<Missing country>";
                                c.PostalCode = !reader.IsDBNull(reader.GetOrdinal("PostalCode")) ? reader.GetString(4) : "<Missing postal code>";
                                c.Phone = !reader.IsDBNull(reader.GetOrdinal("Phone")) ? reader.GetString(5) : "<Missing phone>";
                                c.Email = !reader.IsDBNull(reader.GetOrdinal("Email")) ? reader.GetString(6) : "<Missing Email>";
                                
                                Console.WriteLine(String.Format("{0,2} | {1,15} | {2,15} | {3,15} | {4,22}| {5,20} | {6,20}", reader.GetInt32(0), c.FirstName, c.LastName, c.Country, c.PostalCode, c.Phone, c.Email));
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
            return c;
        }
        /// <summary>
        /// Gets all customers in database.
        /// </summary>
        /// <returns>An Enumerable list of customers. If no customers exist writes Console error message. If failed throws SqlException and shows error message. </returns>
        public IEnumerable<Customer> GetAllCustomers()
        {
            List<Customer> customers = new List<Customer>();

            try
            {
                using (SqlConnection connection = new SqlConnection(DbContext.GetConnectionString()))
                {

                    connection.Open();

                    String sql = $"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            Console.WriteLine(String.Format("{0,0} | {1,8} | {2,14} | {3,14} | {4,22}| {5,20} | {6,20}", reader.GetName(0), reader.GetName(1), reader.GetName(2), reader.GetName(3), reader.GetName(4), reader.GetName(5), reader.GetName(6)));
                            Console.WriteLine("-------------------------------------------------------------------------------------------------------------------------------");

                            if (!reader.HasRows) Console.WriteLine("No customers in database!");
                            while (reader.Read())
                            {
                                Customer c = new Customer();
                                c.CustomerId = reader.GetInt32(0);
                                c.FirstName = !reader.IsDBNull(reader.GetOrdinal("FirstName")) ? reader.GetString(1) : "<Missing first name>";
                                c.LastName = !reader.IsDBNull(reader.GetOrdinal("LastName")) ? reader.GetString(2) : "<Missing last name>";
                                c.Country = !reader.IsDBNull(reader.GetOrdinal("Country")) ? reader.GetString(3) : "<Missing country>";
                                c.PostalCode = !reader.IsDBNull(reader.GetOrdinal("PostalCode")) ? reader.GetString(4) : "<Missing postal code>";
                                c.Phone = !reader.IsDBNull(reader.GetOrdinal("Phone")) ? reader.GetString(5) : "<Missing phone>";
                                c.Email = !reader.IsDBNull(reader.GetOrdinal("Email")) ? reader.GetString(6) : "<Missing Email>";
                                customers.Add(c);


                                Console.WriteLine(String.Format("{0,2} | {1,15} | {2,15} | {3,15} | {4,22}| {5,20} | {6,20}", reader.GetInt32(0), c.FirstName, c.LastName, c.Country, c.PostalCode, c.Phone, c.Email));
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
            return customers;
        }
        /// <summary>
        /// Gets customers matching their First Name to searchString (case insensitive).
        /// </summary>
        /// <param name="searchString">The search parameter for FirstName.</param>
        /// <returns>An Enumerable list of customers matching the searchString in their First Name. If failed throws SqlException and shows error message.</returns>
        public IEnumerable<Customer> GetCustomerByFirstName(string searchString)
        {
            List<Customer> customers = new List<Customer>();

            try
            {
                using (SqlConnection connection = new SqlConnection(DbContext.GetConnectionString()))
                {

                    connection.Open();

                    String sql = $"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE FirstName LIKE '{searchString}%' ";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            Console.WriteLine(String.Format("{0,0} | {1,8} | {2,14} | {3,14} | {4,22}| {5,20} | {6,20}", reader.GetName(0), reader.GetName(1), reader.GetName(2), reader.GetName(3), reader.GetName(4), reader.GetName(5), reader.GetName(6)));
                            Console.WriteLine("-------------------------------------------------------------------------------------------------------------------------------");

                            if (!reader.HasRows) Console.WriteLine($"No customers firstnames matching string '{searchString}'!");

                            while (reader.Read())
                            {
                                Customer c = new Customer();
                                c.CustomerId = !reader.IsDBNull(reader.GetOrdinal("CustomerId")) ? reader.GetInt32(0) : 0;
                                c.FirstName = !reader.IsDBNull(reader.GetOrdinal("FirstName")) ? reader.GetString(1) : "<Missing first name>";
                                c.LastName = !reader.IsDBNull(reader.GetOrdinal("LastName")) ? reader.GetString(2) : "<Missing last name>";
                                c.Country = !reader.IsDBNull(reader.GetOrdinal("Country")) ? reader.GetString(3) : "<Missing country>";
                                c.PostalCode = !reader.IsDBNull(reader.GetOrdinal("PostalCode")) ? reader.GetString(4) : "<Missing postal code>";
                                c.Phone = !reader.IsDBNull(reader.GetOrdinal("Phone")) ? reader.GetString(5) : "<Missing phone>";
                                c.Email = !reader.IsDBNull(reader.GetOrdinal("Email")) ? reader.GetString(6) : "<Missing Email>";
                                
                                Console.WriteLine(String.Format("{0,2} | {1,15} | {2,15} | {3,15} | {4,22}| {5,20} | {6,20}", reader.GetInt32(0), c.FirstName, c.LastName, c.Country, c.PostalCode, c.Phone, c.Email));
                                customers.Add(c);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
            return customers;
        }

        /// <summary>
        /// Gets all customers and selects based on specified OFFSET and FETCH NEXT values. 
        /// </summary>
        /// <param name="offset">Specifies the number of rows to skip before starting to return rows from the query.</param>
        /// <param name="fetch">Specifies the number of rows to return after the OFFSET clause has been processed.</param>
        /// <returns>An Enumerable list of customers. If failed throws SqlException and shows error message.</returns>
        public IEnumerable<Customer> GetCustomersByOffsetAndFetch(int offset, int fetch)
        {
            List<Customer> customers = new List<Customer>();

            try
            {
                using (SqlConnection connection = new SqlConnection(DbContext.GetConnectionString()))
                {

                    connection.Open();

                    String sql = $"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer ORDER BY CustomerId Asc OFFSET {offset} ROWS FETCH NEXT {fetch} ROWS ONLY";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            Console.WriteLine(String.Format("{0,0} | {1,8} | {2,14} | {3,14} | {4,22} | {5,20} | {6,20}", reader.GetName(0), reader.GetName(1), reader.GetName(2), reader.GetName(3), reader.GetName(4), reader.GetName(5), reader.GetName(6)));
                            Console.WriteLine("-------------------------------------------------------------------------------------------------------------------------------");

                            if (!reader.HasRows) Console.WriteLine($"No customers matching the specified offset and fetch criteria!");

                            while (reader.Read())
                            {
                                Customer c = new Customer();
                                c.CustomerId = reader.GetInt32(0);
                                c.FirstName = !reader.IsDBNull(reader.GetOrdinal("FirstName")) ? reader.GetString(1) : "<Missing first name>";
                                c.LastName = !reader.IsDBNull(reader.GetOrdinal("LastName")) ? reader.GetString(2) : "<Missing last name>";
                                c.Country = !reader.IsDBNull(reader.GetOrdinal("Country")) ? reader.GetString(3) : "<Missing country>";
                                c.PostalCode = !reader.IsDBNull(reader.GetOrdinal("PostalCode")) ? reader.GetString(4) : "<Missing postal code>";
                                c.Phone = !reader.IsDBNull(reader.GetOrdinal("Phone")) ? reader.GetString(5) : "<Missing phone>";
                                c.Email = !reader.IsDBNull(reader.GetOrdinal("Email")) ? reader.GetString(6) : "<Missing Email>";
                                customers.Add(c);
                                Console.WriteLine(String.Format("{0,2} | {1,15} | {2,15} | {3,15} | {4,22} | {5,20} | {6,20}", reader.GetInt32(0), c.FirstName, c.LastName, c.Country, c.PostalCode, c.Phone, c.Email));
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
            return customers;
        }
        /// <summary>
        /// Inserts the provided Customer object into the database. Please ignore declaring CustomerID as its AutoIncremented. Check Parameter for values supported.
        /// </summary>
        /// <param name="customer">Leave CustomerId undeclared as its AutoIncremented. Values supported: FirstName, LastName, Country, PostalCode, Phone, Email</param>
        /// <returns>True if affected rows are equal to 1. If failed throws SqlException and shows error message.</returns>
        public bool InsertCustomer(Customer customer)
        {
            int rowsAffected = 0;
            try
            {
                using (SqlConnection connection = new SqlConnection(DbContext.GetConnectionString()))
                {

                    connection.Open();
                    string sql = "INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email) VALUES (@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@FirstName", $"{customer.FirstName}");
                        command.Parameters.AddWithValue("@LastName", $"{customer.LastName}");
                        command.Parameters.AddWithValue("@Country", $"{customer.Country}");
                        command.Parameters.AddWithValue("@PostalCode", $"{customer.PostalCode}");
                        command.Parameters.AddWithValue("@Phone", $"{customer.Phone}");
                        command.Parameters.AddWithValue("@Email", $"{customer.Email}");

                        rowsAffected = command.ExecuteNonQuery();
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }

            if (rowsAffected == 1)
            {
                return true;
            }
            else return false;
        }

        /// <summary>
        /// Updates the Customer object in the database matching the Customer property CustomerID. Updates only non null fields. CustomerId required. Check parameter for values supported.
        /// </summary>
        /// <param name="customer">The Customer with CustomerId declared and declared fields to update in database. 
        /// Values supported: FirstName, LastName, Country, PostalCode, Phone, Email</param>
        /// <returns>True if affected rows are larger or equal to 1. If failed throws SqlException and shows error message.</returns>
        public bool UpdateCustomer(Customer customer)
        {
            int rowsAffected = 0;
            try
            {
                using (SqlConnection connection = new SqlConnection(DbContext.GetConnectionString()))
                {
                    connection.Open();

                    if (customer.FirstName is not null)
                    {
                        string ChangeFirstNamesql = $"UPDATE Customer SET FirstName = @FirstName WHERE CustomerId = {customer.CustomerId}";

                        using (SqlCommand command = new SqlCommand(ChangeFirstNamesql, connection))
                        {
                            command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                            rowsAffected += command.ExecuteNonQuery();
                        }
                    }
                    if (customer.LastName is not null)
                    {
                        string ChangeLastNamesql = $"UPDATE Customer SET LastName = @LastName WHERE CustomerId = {customer.CustomerId}";

                        using (SqlCommand command = new SqlCommand(ChangeLastNamesql, connection))
                        {
                            command.Parameters.AddWithValue("@LastName", customer.LastName);
                            rowsAffected += command.ExecuteNonQuery();
                        }
                    }
                    if (customer.Country is not null)
                    {
                        string ChangeCountrysql = $"UPDATE Customer SET Country = @Country WHERE CustomerId = {customer.CustomerId}";

                        using (SqlCommand command = new SqlCommand(ChangeCountrysql, connection))
                        {
                            command.Parameters.AddWithValue("@Country", customer.Country);
                            rowsAffected += command.ExecuteNonQuery();
                        }
                    }
                    if (customer.PostalCode is not null)
                    {
                        string ChangePostalCodesql = $"UPDATE Customer SET PostalCode = @PostalCode WHERE CustomerId = {customer.CustomerId}";

                        using (SqlCommand command = new SqlCommand(ChangePostalCodesql, connection))
                        {
                            command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                            rowsAffected += command.ExecuteNonQuery();
                        }
                    }
                    if (customer.Phone is not null)
                    {
                        string ChangePhonesql = $"UPDATE Customer SET Phone = @Phone WHERE CustomerId = {customer.CustomerId}";

                        using (SqlCommand command = new SqlCommand(ChangePhonesql, connection))
                        {
                            command.Parameters.AddWithValue("@Phone", customer.Phone);
                            rowsAffected += command.ExecuteNonQuery();
                        }
                    }
                    if (customer.Email is not null)
                    {
                        string ChangeEmailsql = $"UPDATE Customer SET Email = @Email WHERE CustomerId = {customer.CustomerId}";

                        using (SqlCommand command = new SqlCommand(ChangeEmailsql, connection))
                        {
                            command.Parameters.AddWithValue("@Email", customer.Email);
                            rowsAffected += command.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }

            if (rowsAffected >= 1)
            {
                return true;
            }
            else return false;

        }

        /// <summary>
        /// Gets each country's customer count.
        /// </summary>
        /// <returns>An Enumerable list of CustomerCountry. If no customers with country present returns Console error message.
        /// If failed throws SqlException and shows error message</returns>
        public IEnumerable<CustomerCountry> GetCustomerCountInCountries()
        {
            List<CustomerCountry> customers = new List<CustomerCountry>();

            try
            {
                using (SqlConnection connection = new SqlConnection(DbContext.GetConnectionString()))
                {

                    connection.Open();

                    String sql = $"SELECT Country, COUNT(CustomerId) AS Customers FROM Customer GROUP BY Country ORDER BY COUNT(CustomerId) DESC";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (!reader.HasRows) Console.WriteLine("No customers with countries in database!");

                            Console.WriteLine(String.Format("{0,15} | {1,5}", reader.GetName(0), reader.GetName(1)));
                            Console.WriteLine("-----------------------------");

                            while (reader.Read())
                            {
                                CustomerCountry c = new CustomerCountry()
                                {
                                    CountryName = reader.GetString(0),
                                    Customers = reader.GetInt32(1)
                                };

                                customers.Add(c);

                                Console.WriteLine(String.Format("{0,15} | {1,5}", reader.GetString(0), reader.GetInt32(1)));
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
            return customers;
        }
        /// <summary>
        /// Gets all customers grouped by total spent generated by invoices.
        /// </summary>
        /// <returns>An Enumerable list of CustomerSpender.  If no customer matches returns Console error message.
        /// If failed throws SqlException and shows error message</returns>
        public IEnumerable<CustomerSpender> GetCustomersByTotalSpent()
        {
            List<CustomerSpender> customers = new List<CustomerSpender>();

            try
            {
                using (SqlConnection connection = new SqlConnection(DbContext.GetConnectionString()))
                {

                    connection.Open();

                    String sql = $"SELECT c.CustomerId, SUM(i.Total) AS TotalSpending FROM Customer c JOIN Invoice i  ON c.CustomerId = i.CustomerId GROUP BY c.CustomerId ORDER BY SUM(i.Total) DESC";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {

                            if (!reader.HasRows) Console.WriteLine("No customers in database!");

                            Console.WriteLine(String.Format("{0,15} | {1,5}", reader.GetName(0), reader.GetName(1)));
                            Console.WriteLine("-----------------------------");

                            while (reader.Read())
                            {
                                CustomerSpender c = new CustomerSpender()
                                {
                                    CustomerId = reader.GetInt32(0),
                                    TotalSpending = reader.GetDecimal(1)
                                };

                                customers.Add(c);


                                Console.WriteLine(String.Format("{0,15} | {1,5}", reader.GetInt32(0), reader.GetDecimal(1)));
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
            return customers;
        }

        /// <summary>
        /// Gets the Customer's most popular genre corresponding to most tracks from invoices associated to the customer. 
        /// In case of a tie displays both genre's.
        /// </summary>
        /// <param name="customerId">The customer ID used to query the database.</param>
        /// <returns>An Enumerable list of CustomerGenre corresponding to most track from invocies 
        /// matching the searchString in their First Name. If no customer matches returns Console error message.
        ///If failed throws SqlException and shows error message</returns>
        public IEnumerable<CustomerGenre> GetCustomerMostPopularGenre(int customerId)
        {
            List<CustomerGenre> customerGenres = new List<CustomerGenre>();

            try
            {
                using (SqlConnection connection = new SqlConnection(DbContext.GetConnectionString()))
                {

                    connection.Open();

                    String sql = $"DROP TABLE IF EXISTS table1; SELECT * INTO table1 FROM (SELECT DISTINCT c.CustomerId, g.GenreId AS Most_Popular_GenreId, COUNT(g.GenreId) AS Genre_Occurence FROM Customer c INNER JOIN Invoice i ON i.CustomerId = c.CustomerId INNER JOIN InvoiceLine l ON l.InvoiceId = i.InvoiceId INNER JOIN Track t ON l.TrackId = t.TrackId INNER JOIN Genre g ON g.GenreId = t.GenreId WHERE c.CustomerId = {customerId} GROUP BY g.GenreId, c.CustomerId) AS t1; SELECT Most_Popular_GenreId FROM table1 WHERE Genre_Occurence = (SELECT MAX(Genre_Occurence) FROM table1);";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {

                            if (!reader.HasRows) Console.WriteLine("No customers in database matching that!");

                            Console.WriteLine(String.Format("{0,15} | {1,5}", "CustomerId", reader.GetName(0)));
                            Console.WriteLine("-----------------------------");

                            while (reader.Read())
                            {
                                CustomerGenre c = new CustomerGenre()
                                {
                                    CustomerId = customerId,
                                    GenreId = reader.GetInt32(0)
                                };

                                customerGenres.Add(c);


                                Console.WriteLine(String.Format("{0,15} | {1,5}", customerId, reader.GetInt32(0)));
                                Console.WriteLine("-----------------------------");
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
            return customerGenres;
        }



    }
}

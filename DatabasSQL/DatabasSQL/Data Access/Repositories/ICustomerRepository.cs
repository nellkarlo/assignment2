﻿using DatabasSQL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabasSQL.Data_Access.Repositories
{
    public interface ICustomerRepository
    {
        IEnumerable<Customer> GetAllCustomers();
        Customer GetCustomerById(int CustomerID);
        bool InsertCustomer(Customer customer);
        bool UpdateCustomer(Customer customer);
        IEnumerable<Customer> GetCustomersByOffsetAndFetch(int offset, int fetch);
        IEnumerable<Customer> GetCustomerByFirstName(string InputCustomerName);
        IEnumerable<CustomerCountry> GetCustomerCountInCountries();
        IEnumerable<CustomerSpender> GetCustomersByTotalSpent();
        IEnumerable<CustomerGenre> GetCustomerMostPopularGenre(int customerId);

    }
}
